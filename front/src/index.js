import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import "picnic/picnic.min.css";

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();


//This is the same thing as the below,
/*const displayContacts =async ()=> 
  {
const response = await fetch('//localhost:8080/contacts/list');
const text =await response.text();
console.log(text);


  }
  displayContacts();*/

  // front/src/index.js
fetch('//localhost:8080/contacts/list')
.then( response => response.text() )
.then( text => console.log("we got a response!!",text))
